      var obsData={};

      obsData['st_g']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth','HadCRUT4','GISS','NOAA'];
      obsData['st_g_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth'];
      obsData['st_g_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth'];

      obsData['st_l']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth','HadCRUT4','GISS','NOAA','CRU-TS','U.Delaware'];
      obsData['st_l_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth','CRU-TS','U.Delaware'];
      obsData['st_l_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth','CRU-TS','U.Delaware'];

      obsData['st_a']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','CRU-TS','U.Delaware'];
      obsData['st_a_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','CRU-TS','U.Delaware'];
      obsData['st_a_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','CRU-TS','U.Delaware'];

      obsData['st_o']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth','HadCRUT4','GISS','NOAA'];
      obsData['st_o_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth'];
      obsData['st_o_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2','Berkeley-Earth'];

      obsData['st_nhp']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];
      obsData['st_nhp_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];
      obsData['st_nhp_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];

      obsData['st_nhp']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];
      obsData['st_nhp_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];
      obsData['st_nhp_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];

      obsData['st_shp']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];
      obsData['st_shp_ac']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];
      obsData['st_shp_mon']=['ERA5','ERA-Interim','ERA40','MERRA','MERRA-2','NCEP2'];

      obsData['stmx_g']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmx_g_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmx_g_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['stmx_l']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmx_l_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmx_l_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['stmx_o']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmx_o_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmx_o_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['stmn_g']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmn_g_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmn_g_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['stmn_l']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmn_l_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmn_l_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['stmn_o']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmn_o_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['stmn_o_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['dtr_g']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['dtr_g_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['dtr_g_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['dtr_l']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['dtr_l_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['dtr_l_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['dtr_o']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['dtr_o_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['dtr_o_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['gt_g']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['gt_g_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['gt_g_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['gt_l']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['gt_l_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['gt_l_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['gt_o']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['gt_o_ac']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['gt_o_mon']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['tg1']=['ERA-Interim','ERA40','ERA-Interim-Land'];
      obsData['tg1_ac']=['ERA-Interim','ERA40','ERA-Interim-Land'];
      obsData['tg1_mon']=['ERA-Interim','ERA40','ERA-Interim-Land'];

      obsData['gto_o']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['gto_o_ac']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['gto_o_mon']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];

      obsData['gtt_w']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['gtt_w_ac']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['gtt_w_mon']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];

      obsData['gto_ot']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['gto_ot_ac']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['gto_ot_mon']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];

      obsData['sst_glb']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['sst_glb_ac']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['sst_glb_mon']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['sst_nhe']=['ERA-Interim'];
      obsData['sst_nhe_ac']=['ERA-Interim'];
      obsData['sst_nhe_mon']=['ERA-Interim'];
      obsData['sst_she']=['ERA-Interim'];
      obsData['sst_she_ac']=['ERA-Interim'];
      obsData['sst_she_mon']=['ERA-Interim'];
      obsData['sst_tro']=['ERA-Interim'];
      obsData['sst_tro_ac']=['ERA-Interim'];
      obsData['sst_tro_mon']=['ERA-Interim'];

      obsData['temp_l5']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['temp_l5_ac']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];
      obsData['temp_l5_mon']=['ERA-Interim','ERA40','HadISST','OISST','ERSST3','ERSST4','AMIP-5','AMIP-6'];

      obsData['srh_g']=['ERA-Interim'];
      obsData['srh_g_ac']=['ERA-Interim'];
      obsData['srh_g_mon']=['ERA-Interim'];

      obsData['srh_l']=['ERA-Interim'];
      obsData['srh_l_ac']=['ERA-Interim'];
      obsData['srh_l_mon']=['ERA-Interim'];

      obsData['srh_o']=['ERA-Interim'];
      obsData['srh_o_ac']=['ERA-Interim'];
      obsData['srh_o_mon']=['ERA-Interim'];

      obsData['sq_g']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['sq_g_ac']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['sq_g_mon']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['sq_l']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['sq_l_ac']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['sq_l_mon']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['sq_o']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['sq_o_ac']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['sq_o_mon']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['cldt_g']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldt_g_ac']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldt_g_mon']=['ERA5','ERA-Interim','NCEP2','MERRA'];

      obsData['cldt_l']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldt_l_ac']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldt_l_mon']=['ERA5','ERA-Interim','NCEP2','MERRA'];

      obsData['cldt_o']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldt_o_ac']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldt_o_mon']=['ERA5','ERA-Interim','NCEP2','MERRA'];

      obsData['cldo_g']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldo_g_ac']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldo_g_mon']=['ERA5','ERA-Interim','NCEP2','MERRA'];

      obsData['cldo_l']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldo_l_ac']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldo_l_mon']=['ERA5','ERA-Interim','NCEP2','MERRA'];

      obsData['cldo_o']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldo_o_ac']=['ERA5','ERA-Interim','NCEP2','MERRA'];
      obsData['cldo_o_mon']=['ERA5','ERA-Interim','NCEP2','MERRA'];

      obsData['olr']=['ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['fss_g']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['fss_l']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];
      obsData['fss_o']=['ERA5','ERA-Interim','NCEP2','MERRA','MERRA-2'];

      obsData['fsg_g']=['ERA5','ERA-Interim'];
      obsData['fsg_l']=['ERA5','ERA-Interim'];
      obsData['fsg_o']=['ERA5','ERA-Interim'];

      obsData['fsgc_g']=['ERA-Interim'];
      obsData['fsgc_l']=['ERA-Interim'];
      obsData['fsgc_o']=['ERA-Interim'];

      obsData['fsrg_g']=['ERA5','ERA-Interim'];
      obsData['fsrg_l']=['ERA5','ERA-Interim'];
      obsData['fsrg_o']=['ERA5','ERA-Interim'];

      obsData['salb_g']=['ERA5','ERA-Interim'];
      obsData['salb_l']=['ERA5','ERA-Interim'];
      obsData['salb_o']=['ERA5','ERA-Interim'];

      obsData['fdl_g']=['ERA5','ERA-Interim'];
      obsData['fdl_l']=['ERA5','ERA-Interim'];
      obsData['fdl_o']=['ERA5','ERA-Interim'];

      obsData['flg_g']=['ERA5','ERA-Interim'];
      obsData['flg_l']=['ERA5','ERA-Interim'];
      obsData['flg_o']=['ERA5','ERA-Interim'];

      obsData['flgc_g']=['ERA-Interim'];
      obsData['flgc_l']=['ERA-Interim'];
      obsData['flgc_o']=['ERA-Interim'];

      obsData['flrg_g']=['ERA-Interim'];
      obsData['flrg_l']=['ERA-Interim'];
      obsData['flrg_o']=['ERA-Interim'];

      obsData['hfs_g']=['ERA5','ERA-Interim','MERRA-2'];
      obsData['hfs_l']=['ERA5','ERA-Interim','MERRA-2'];
      obsData['hfs_o']=['ERA5','ERA-Interim','MERRA-2'];

      obsData['hfl_g']=['ERA5','ERA-Interim','MERRA-2'];
      obsData['hfl_l']=['ERA5','ERA-Interim','MERRA-2'];
      obsData['hfl_o']=['ERA5','ERA-Interim','MERRA-2'];

      obsData['hfsl_g']=['ERA5','ERA-Interim','MERRA-2'];
      obsData['hfsl_l']=['ERA5','ERA-Interim','MERRA-2'];
      obsData['hfsl_o']=['ERA5','ERA-Interim','MERRA-2'];

      obsData['pwat_g']=['ERA-Interim'];
      obsData['pwat_l']=['ERA-Interim'];
      obsData['pwat_o']=['ERA-Interim'];

      obsData['sss_glb']=['Levitus(ann.clim.)'];
      obsData['sss_glb_mon']=['Levitus(ann.clim.)'];
      obsData['sss_glb_ac']=['Levitus(ann.clim.)'];
      obsData['salt_l5']=['Levitus(ann.clim.)'];
      obsData['salt_l5_mon']=['Levitus(ann.clim.)'];
      obsData['salt_l5_ac']=['Levitus(ann.clim.)'];

      obsData['pcp_g']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_g_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_g_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];

      obsData['pcp_l']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2','GPCC','CRU-TS','U.Delaware'];
      obsData['pcp_l_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2','GPCC','CRU-TS','U.Delaware'];
      obsData['pcp_l_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2','GPCC','CRU-TS','U.Delaware'];

      obsData['pcp_a']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2','GPCC','CRU-TS','U.Delaware'];
      obsData['pcp_a_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2','GPCC','CRU-TS','U.Delaware'];
      obsData['pcp_a_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2','GPCC','CRU-TS','U.Delaware'];

      obsData['pcp_o']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_o_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_o_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];

      obsData['pcp_nh']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_nh_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_nh_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];

      obsData['pcp_sh']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_sh_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_sh_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];

      obsData['pcp_tr']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_tr_ac']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];
      obsData['pcp_tr_mon']=['ERA5','GPCP2.3','CMAP','ERA-Interim','MERRA','MERRA-2'];

      obsData['pcpn_g']=['ERA-Interim'];
      obsData['pcpn_l']=['ERA-Interim'];
      obsData['pcpn_o']=['ERA-Interim'];

      obsData['qfs_g']=['ERA-Interim'];
      obsData['qfs_l']=['ERA-Interim'];
      obsData['qfs_o']=['ERA-Interim'];

      obsData['pme_g']=['ERA-Interim'];
      obsData['pme_l']=['ERA-Interim'];
      obsData['pme_o']=['ERA-Interim'];

      obsData['vfsm_l']=['ERA-Interim'];

      obsData['rof_o']=['ERA-Interim'];

      obsData['sicna_o_03_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_o_06_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_o_09_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_o_12_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_o_ac_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_o_mon_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_o_ann_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];

      obsData['sicna_o_03_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_o_06_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_o_09_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_o_12_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_o_ac_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_o_mon_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_o_ann_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];

      obsData['sicna_nemo_o_03_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_nemo_o_06_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_nemo_o_09_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_nemo_o_12_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_nemo_o_ac_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_nemo_o_mon_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicna_nemo_o_ann_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];

      obsData['sicna_nemo_o_03_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_nemo_o_06_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_nemo_o_09_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_nemo_o_12_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_nemo_o_ac_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_nemo_o_mon_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicna_nemo_o_ann_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];


      obsData['sicne_o_03_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_o_06_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_o_09_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_o_12_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_o_ac_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_o_mon_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_o_ann_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];

      obsData['sicne_o_03_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_o_06_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_o_09_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_o_12_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_o_ac_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_o_mon_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_o_ann_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];

      obsData['sicne_nemo_o_03_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_nemo_o_06_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_nemo_o_09_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_nemo_o_12_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_nemo_o_ac_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_nemo_o_mon_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sicne_nemo_o_ann_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];

      obsData['sicne_nemo_o_03_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_nemo_o_06_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_nemo_o_09_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_nemo_o_12_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_nemo_o_ac_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_nemo_o_mon_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sicne_nemo_o_ann_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];


      obsData['sice_o_03_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sice_o_06_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sice_o_09_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sice_o_12_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sice_o_ac_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sice_o_mon_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];
      obsData['sice_o_ann_nh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0','PIOMAS'];

      obsData['sice_o_03_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sice_o_06_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sice_o_09_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sice_o_12_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sice_o_ac_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sice_o_mon_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];
      obsData['sice_o_ann_sh']=['ERA5','ERA-Interim','ERA40','NSIDC','NASAteam','BOOTstrap','AMIP-5','AMIP-6','HadISST1','HadISST2','CFSR','NCEP','ORAP5.0'];

      obsData['sicv_o_03_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_o_06_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_o_09_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_o_12_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_o_ac_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_o_mon_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_o_ann_nh']=['PIOMAS','CFSR','ORAP5'];

      obsData['sicv_o_03_sh']=['CFSR','ORAP5'];
      obsData['sicv_o_06_sh']=['CFSR','ORAP5'];
      obsData['sicv_o_09_sh']=['CFSR','ORAP5'];
      obsData['sicv_o_12_sh']=['CFSR','ORAP5'];
      obsData['sicv_o_ac_sh']=['CFSR','ORAP5'];
      obsData['sicv_o_mon_sh']=['CFSR','ORAP5'];
      obsData['sicv_o_ann_sh']=['CFSR','ORAP5'];


      obsData['sicv_nemo_o_03_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_nemo_o_06_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_nemo_o_09_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_nemo_o_12_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_nemo_o_ac_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sicv_nemo_o_mon_nh']=['PIOMAS','CFSR','ORAP5'];

      obsData['sicv_nemo_o_03_sh']=['CFSR','ORAP5'];
      obsData['sicv_nemo_o_06_sh']=['CFSR','ORAP5'];
      obsData['sicv_nemo_o_09_sh']=['CFSR','ORAP5'];
      obsData['sicv_nemo_o_12_sh']=['CFSR','ORAP5'];
      obsData['sicv_nemo_o_ac_sh']=['CFSR','ORAP5'];
      obsData['sicv_nemo_o_mon_sh']=['CFSR','ORAP5'];


      obsData['sich_o_03_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_o_06_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_o_09_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_o_12_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_o_ac_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_o_mon_nh']=['PIOMAS','CFSR','ORAP5'];

      obsData['sich_o_03_sh']=['CFSR','ORAP5'];
      obsData['sich_o_06_sh']=['CFSR','ORAP5'];
      obsData['sich_o_09_sh']=['CFSR','ORAP5'];
      obsData['sich_o_12_sh']=['CFSR','ORAP5'];
      obsData['sich_o_ac_sh']=['CFSR','ORAP5'];
      obsData['sich_o_mon_sh']=['CFSR','ORAP5'];


      obsData['sich_nemo_o_03_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_nemo_o_06_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_nemo_o_09_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_nemo_o_12_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_nemo_o_ac_nh']=['PIOMAS','CFSR','ORAP5'];
      obsData['sich_nemo_o_mon_nh']=['PIOMAS','CFSR','ORAP5'];

      obsData['sich_nemo_o_03_sh']=['CFSR','ORAP5'];
      obsData['sich_nemo_o_06_sh']=['CFSR','ORAP5'];
      obsData['sich_nemo_o_09_sh']=['CFSR','ORAP5'];
      obsData['sich_nemo_o_12_sh']=['CFSR','ORAP5'];
      obsData['sich_nemo_o_ac_sh']=['CFSR','ORAP5'];
      obsData['sich_nemo_o_mon_sh']=['CFSR','ORAP5'];


      obsData['sicm_o']=['CFSR','ORAP5'];
      obsData['sicm_o_ac']=['CFSR','ORAP5'];
      obsData['sicm_o_mon']=['CFSR','ORAP5'];

      obsData['nino34_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino34_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino34_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino34_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino34_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nino3_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino3_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino3_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino3_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino3_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nino4_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino4_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino4_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino4_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino4_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nino12_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino12_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino12_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino12_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nino12_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['gtnino34_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino34_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino34_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino34_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino34_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['gtnino3_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino3_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino3_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino3_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino3_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['gtnino4_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino4_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino4_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino4_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino4_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['gtnino12_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino12_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino12_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino12_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['gtnino12_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nemonino34_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino34_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino34_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino34_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino34_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nemonino3_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino3_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino3_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino3_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino3_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nemonino4_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino4_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino4_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino4_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino4_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      obsData['nemonino12_mon_spc']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino12_mon_ac']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino12_mon_sd']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino12_mon_ano']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];
      obsData['nemonino12_mon']=['ERA5(1950-)','ERSST5(1854-)','CMIP6(1870-)'];

      function obsList()
      {
	var varname=document.getElementById("varname");
	var selvarname=varname.options[varname.selectedIndex].value;
	var obss=obsData[selvarname];
	if (obss){
	  var txt = "<b>Observations:</b> ";
	  for (var i=0; i < obss.length ; i++) {
             txt = txt + (i+1) + "=" + obss[i] + "; ";
	  }
	  document.getElementById("obslist").innerHTML=txt;
	}else{
	  document.getElementById("obslist").innerHTML="No observations are available.";
	}
      }

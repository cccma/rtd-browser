#include <stdio.h>

void buffskip_(int *fd, int *length, int *status)

/* 2003 March 30 Alan W. Irwin 
 * Modified version of buffin that simply skips the big_endian
 * record rather than reading it.
 * 
 * Jan 17/92 - E. Chan
 *
 * buffin - reads Fortran generated standard unformatted records on 32 bit 
 *          machines
 * 
 * Author - E. Chan
 *
 * "Buffin" reads a record from file descriptor "fd" written using a Fortran
 * unformatted write statement. The record is stored in the array "buffer"
 * as integers. The maximum length of the array must be specified on invocation
 * of the function through the "length" argument. "Length" will contain the 
 * actual length of the record read on return to the calling routine. 
 *
 * Standard Fortran sequential file unformatted records are preceded and 
 * followed by 4 bytes of data that indicate the byte length of the record. 
 * "Buffin" reads the leading 4 bytes to ascertain the record length before 
 * actually reading the record. 
 * 
 * The file descriptor "fd" is the C equivalent of Fortran unit numbers. Since
 * descriptors 0, 1, and 2 are reserved for standard input, output, and error,
 * respectively, all Fortran units explicitly opened are given descriptors
 * beginning with the number 3. The assignment of descriptors is in the order
 * in which the files were opened (e.g. if Fortran units 7, 2, and 3 were 
 * opened in that order in the calling routine, then the respective 
 * descriptors are: 3, 4, and 5).
 *
 * The "status" returned may be one of the following:
 *
 *       0      --      successful completion of a full record read operation
 *                      without encountering the end of the current record
 *                      (length of record is less than or equal to the maximum
 *                      specified)
 *
 *       1      --      end of file encountered during read operation
 *
 *       2      --      unexpected problem during the read operation
 *
 * Example:
 *
 * In Fortran program: 
 *
 * CALL BUFFSKIP(3,MAX,K)
 *
 * One record is skipped from the file associated with the 
 * C file descriptor 3. The only significance of MAX is to keep track
 * of the size of the record that was skipped.  The status of the read 
 * operation is returned through K.
 *
 */

{
        char *buf;
        char buf_big[4];
        int size, size_begin, size_end, max, offset, n, ibyte;
/*
 * Set the character pointer "buff" to point to the same location as the 
 * integer pointer "buffer". Also, set the character pointer "buf" to point 
 * to the integer "size".
 *
 */
        buf  = (char *) &size;
/*
 * Read the first 4 bytes from the unformatted record (which contains the 
 * byte length of the record). Also check for problems encountered during read.
 * If the end of the file is reached, set the status to one and return to  
 * the calling routine.
 *
 */
        n = read (*fd, buf_big, 4);

        if ( n == 0 ) {
                *status = 1;
                *length = 0;
                return;
        } else if ( n == -1 ) {
                *status = 2;
                *length = 0;
//                fprintf (stderr, "WarningA: problem occurred while reading record\n");
                return;
        } /* endif */
/*
 * Convert big-endian size to little-endian.
 */
        buf[3] = buf_big[0];
        buf[2] = buf_big[1];
        buf[1] = buf_big[2];
        buf[0] = buf_big[3];
        size_begin = size;
/*
 * Calculate the number of integers in the record and return that value.
 */
        *length = size/sizeof(int);
/*
 * Move the read pointer to the beginning of the next record, skipping past 
 * any remaining unread data (this does not include the trailing 4 bytes 
 * of the record which will be subsequently checked).
 */
        offset = size;

        lseek (*fd, offset, 1);
/*
 * Read the last 4 bytes from the unformatted record (which contains the 
 * byte length of the record). Also check for problems encountered during read.
 * If the end of the file is reached, set the status to one and return to  
 * the calling routine.
 *
 */
        n = read (*fd, buf_big, 4);
/*
 * Convert big-endian size to little-endian.
 */
        buf[3] = buf_big[0];
        buf[2] = buf_big[1];
        buf[1] = buf_big[2];
        buf[0] = buf_big[3];
        size_end = size;

        if ( n != 4 ) {
                *status = 2;
                *length = 0;
//                fprintf (stderr, "WarningB: problem occurred while reading record\n");
                return;
        } else if ( size_begin != size_end ) {
                *status = 2;
                *length = 0;
//                fprintf (stderr, "Warning: invalid unformatted record\n");
                return;
        } /* endif */
        *status = 0;

}
